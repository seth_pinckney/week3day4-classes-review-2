package com.techelevator;

/*
 * It is very common to find static methods grouped together in a single "utility" class.
 * Other examples are java.lang.Math and java.util.Arrays
 */
public class Circles {
	
	public static double getArea(double radius) {
		return Math.PI * radius * radius;
	}
	
	public static double getCircumference(double radius) {
		return 2 * Math.PI * radius;
	}
	
	public static double getDiameter(double radius) {
		return 2 * radius;
	}
}