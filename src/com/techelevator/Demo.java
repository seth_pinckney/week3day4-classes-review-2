package com.techelevator;

public class Demo {
	public static void main(String[] args) {
		
		// classes are datatypes, so we can declare variables using a class as the type
		SimplestClassPossible simplest;
		
		// To create a new instance of a class, we use the "new" operator
		simplest = new SimplestClassPossible(); // Here we are calling the constructor SimplestClassPossible( ) 
												// All objects have a no-arg constructor by default
												// it is called a no-arg constructor because it doesn't accept any arguments 
		
		System.out.println("### Using instance variables ###");
		ClassWithInstanceVariables firstInstance = new ClassWithInstanceVariables();
		firstInstance.numberVariable = 2; // values can be assigned to instance variables by writing the name of the instance, dot (i.e. '.'), name of the variable
		firstInstance.stringVariable = "Two";
		int localVariable = firstInstance.numberVariable; // values can be retrieved from instance variables using the same notation
		System.out.println("firstInstance.numberVariable : "+firstInstance.numberVariable);
		System.out.println("firstInstance.stringVariable : "+firstInstance.stringVariable);
		System.out.println("localVariable : "+localVariable);
		
		System.out.println("\n### Instance variables scope is a class instance ###");
		// the scope of an instance variable is an instance of the class it belongs to
		ClassWithInstanceVariables secondInstance = new ClassWithInstanceVariables();
		secondInstance.numberVariable = 5;
		secondInstance.stringVariable = "Five";
		System.out.println("firstInstance.numberVariable : "+firstInstance.numberVariable);
		System.out.println("firstInstance.stringVariable : "+firstInstance.stringVariable);
		System.out.println("secondInstance.numberVariable : "+secondInstance.numberVariable);
		System.out.println("secondInstance.stringVariable : "+secondInstance.stringVariable);
		
		System.out.println("\n### Encapsulation ###");
		DavidWintrich david = new DavidWintrich( );
		//david.firstName = "David";  // this is a compile error because firstName is a private variable
		System.out.println("david.getFirstName() : "+david.getFirstName());
		System.out.println("david.getLastName() : "+david.getLastName());
		
		System.out.println("\n### Constructor Arguments ###");
		Person potus = new Person("Barack", "Obama");
		Person vp = new Person("Joe", "Biden");
		System.out.println("potus.getFirstName() : "+potus.getFirstName());
		System.out.println("potus.getLastName() : "+potus.getLastName());
		System.out.println("veep.getFirstName() : "+vp.getFirstName());
		System.out.println("veep.getLastName() : "+vp.getLastName());
		
		Person somebody = new Person( );
		System.out.println("somebody.getFirstName() : "+somebody.getFirstName());
		System.out.println("somebody.getLastName() : "+somebody.getLastName());
		
		System.out.println("\n### static variables ###");
		Employee programmer = new Employee("Pete Programmer", "IT", 60000);
		Employee manager = new Employee("Maggie Manager", "Sales", 100000);
		Employee janitor = new Employee("John Janitor", "Facilities", 15000);
		
		System.out.println("Employees of "+Employee.THE_COMPANY);
		System.out.println("programmer.getName() : "+programmer.getName());
		System.out.println("programmer.getId() : "+programmer.getId());
		System.out.println("manager.getName() : "+manager.getName());
		System.out.println("manager.getId() : "+manager.getId());
		System.out.println("janitor.getName() : "+janitor.getName());
		System.out.println("janitor.getId() : "+janitor.getId());
		
		System.out.println("\n### static methods ###");
		double radius = 2.5;
		System.out.println("radius is : "+radius);
		System.out.println("diameter is : "+Circles.getDiameter(radius));
		System.out.println("cicumference is : "+Circles.getCircumference(radius));
		System.out.println("area is : "+Circles.getArea(radius));
	}
}
