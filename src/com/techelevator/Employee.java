package com.techelevator;

public class Employee {

	public static final String THE_COMPANY = "ACME Corp";
	
	private static int globalId = 1;  // static variables are known as "class variables" and are shared by all instances of a class
	
	private int id;
	private String name;
	private String department;
	private double salary;

	public Employee(String employeeName, String employeeDepartment, double employeeSalary) {
		// use of the "this" variable is optional for referring to instance variables (if there is not naming ambiguity)
		name = employeeName;				
		department = employeeDepartment;
		salary = employeeSalary;
		
		id = globalId++;
	}

	public String getName() {
		return name;
	}

	public String getDepartment() {
		return department;
	}

	public double getSalary() {
		return salary;
	}
	
	public int getId( ) {
		return id;
	}
}
