package com.techelevator;

public class ClassWithInstanceVariables {
	
	// instance variables are declared within the body of the class definition block
	// all instance variables have an access modifier, a type, and a name
	// variable names should start with a lower-case letter and use CamelCase for multiple words
	public int numberVariable;
	public String stringVariable;
	
}
