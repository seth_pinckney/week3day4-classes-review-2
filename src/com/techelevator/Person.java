package com.techelevator;

public class Person {
	private String firstName;
	private String lastName;
	
	public Person( ) {
		this.firstName = "John";
		this.lastName = "Doe";
	}
	
	// this constructor allows parameters that are used to initialize this class' instance variables
	public Person(String fname, String lname) {  // it would not be unusual to see these constructor arguments named "firstName" and "lastName" (matching the names of the instance variables)
		this.firstName = fname;
		this.lastName = lname;
	}
	
	public String getFirstName() {
		return this.firstName;  // The "this" variable is a reference to the class instance upon which the getFirstName method has been invoked
	}
	
	public String getLastName() {
		return this.lastName;  // The "this" variable is a reference to the class instance upon which the getLastName method has been invoked
	}
}
