package com.techelevator;

public class DavidWintrich {
	
	// The private access modifier means that these instance variables are only visible inside the DavidWintrich class
	private String firstName;
	private String lastName;
	
	// a constructor is a method without a return type
	// the name of the constructor should match the name of the class
	// because the instance variables are private they cannot be initialized from out side this class
	// we're using a constructor here to initialize these variables
	public DavidWintrich( ) {
		// the "this" variable, is a reference to the class instance that is currently being constructed
		// The data type of the "this" variable is DavidWintrich
		this.firstName = "David";
		this.lastName = "Wintrich";
	}
	
	// because the instance variables are private access, we can use public methods to return data from instances of this class
	public String getFirstName( ) {
		return this.firstName;
	}
	
	public String getLastName( ) {
		return this.lastName;
	}
}
