package com.techelevator;

public class SimplestClassPossible { // defines a class named SimplestClassPossible

	// if this class weren't so simple, the definition of this class' member variables and methods would appear in this block
	
}

// The fully qualified class name of this class is com.techelevator.SimplestClassPossible